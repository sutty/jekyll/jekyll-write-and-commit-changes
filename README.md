# jekyll-write-and-commit-changes

When we're migrating data or moving things around, it's better to do it
programatically. This plugin is useful in these cases, allowing us to
save changes to posts and even commiting them to the Git repository.

## Installation

Add this line to your site's Gemfile:

```ruby
gem 'jekyll-write-and-commit-changes'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-write-and-commit-changes

## Usage

Add the plugin to your `_config.yml`:

```yaml
plugins:
- jekyll-write-and-commit-changes
```

If you don't want a post to be saved, add `save: false` to its metadata.
It's useful on other plugins.

### Example

An ad-hoc plugin at `_plugins/change_something.rb`

```ruby
Jekyll::Hooks.register :site, :post_read do |site|
  site.documents.each do |doc|
    doc.data['change'] = 'something'
    doc.save
  end

  site.repository.commit 'Changed something'
end
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-write-and-commit-changes>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-write-and-commit-changes project’s
codebases, issue trackers, chat rooms and mailing lists is expected to
follow the [code of conduct](https://sutty.nl/en/code-of-conduct/).

# frozen_string_literal: true

module Jekyll
  # Super basic access to the Git repository, only useful for commiting
  # changes.
  class Repository
    attr_reader :site, :git, :path

    def initialize(site)
      require 'rugged'

      @site = site
      @path = site.source
      @git  = Rugged::Repository.new(path)
    end

    # Commit changes to repository, first add files to the commit index,
    # then write changes to reflect the current state (otherwise the
    # file will be commited but missing from disk (?)) and then create a
    # commit.
    #
    # @param [String] Commit message
    # @param [Hash] Author
    # @return [String] Commit hash
    def commit(message, author = jekyll_author)
      site.staged_files.each do |file|
        git.index.add(real_path file)
      end

      git.index.write

      Rugged::Commit.create(git, message: message, update_ref: 'HEAD',
                                 parents: [git.head.target],
                                 tree: git.index.write_tree,
                                 author: author,
                                 committer: author)
    end

    def jekyll_author
      @jekyll_author ||= { name: 'Jekyll', email: 'jekyll@localhost', time: Time.now }
    end

    private

    # Find file's real path, relative to site.
    def real_path(file)
      Pathname.new(file).realpath.relative_path_from(path).to_s
    end
  end
end

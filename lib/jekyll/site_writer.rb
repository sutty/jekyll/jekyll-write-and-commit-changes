# frozen_string_literal: true

require 'jekyll/site'

module Jekyll
  module SiteWriter
    def self.included(base)
      base.class_eval do
        # List of files to commit
        #
        # @return [Set]
        def staged_files
          @staged_files ||= Set.new
        end

        # Access the Git repository
        #
        # @return [Jekyll::Repository]
        def repository
          @repository ||= Jekyll::Repository.new(self)
        end
      end
    end
  end
end

Jekyll::Site.include Jekyll::SiteWriter

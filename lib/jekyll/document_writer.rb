# frozen_string_literal: true

require 'jekyll/document'

module Jekyll
  module DocumentWriter
    def self.included(base)
      base.class_eval do
        # Writes changes to the file and adds it to git staged changes
        def save
          return unless data.fetch('save', true)

          Jekyll.logger.debug 'Writing', path

          file = File.open(path, File::RDWR | File::CREAT, 0o640) do |f|
            f.flock(File::LOCK_EX)
            f.rewind

            f.write(sanitized_data.to_yaml)
            f.write("---\n\n")
            f.write(content)

            f.flush
            f.truncate(f.pos)
          end

          site.staged_files << real_path if file.zero?

          file.zero?
        end

        # Resolves the real path for the document
        # @return [String]
        def real_path
          @real_path ||= Pathname.new(relative_path).realpath.relative_path_from(site.source).to_s
        end

        # Prepares the data for dumping, by excluding some keys and
        # transforming other Documents replaced by jekyll-linked-posts.
        #
        # @return [Hash]
        def sanitized_data
          data.reject do |k, _|
            excluded_attributes.include? k
          end.transform_values do |value|
            case value
            when Jekyll::Document
              value.data['uuid']
            when Jekyll::Convertible
              value.data['uuid']
            when Set
              value.map do |v|
                v.respond_to?(:data) ? v.data['uuid'] : v
              end
            when Array
              value.map do |v|
                v.respond_to?(:data) ? v.data['uuid'] : v
              end
            when Hash
              value.transform_values do |v|
                v.respond_to?(:data) ? v.data['uuid'] : v
              end
            else
              value
            end
          end
        end

        def excluded_attributes
          @excluded_attributes ||= %w[slug ext date excerpt]
        end
      end
    end
  end
end

Jekyll::Document.include Jekyll::DocumentWriter
